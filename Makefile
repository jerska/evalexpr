dirs = \
	src

.PHONY : all clean

all:
	$(foreach dir, $(dirs), cd $(dir) && make && cd ..;)

clean:
	$(foreach dir, $(dirs), cd $(dir) && make $@ && cd ..;)
