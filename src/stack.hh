#ifndef STACK_HH_
# define STACK_HH_

template<class ... Args>
struct Stack;

template<class ... Args>
struct StackPush;

template<class ... Args>
struct StackPop;

struct EmptyStack;

# include "stack.hxx"

#endif // STACK_HH_
