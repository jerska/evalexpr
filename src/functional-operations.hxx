#ifndef FUNCTIONAL_OPERATIONS_HXX_
# define FUNCTIONAL_OPERATIONS_HXX_

template<int A, int B>
constexpr int Add()
{
    return A + B;
}

template<int A, int B>
constexpr int Sub()
{
    return A - B;
}

template<int A, int B>
constexpr int Mul()
{
    return A * B;
}

template<int A, int B>
constexpr int Div()
{
    return A / B;
}

#endif // FUNCTIONAL_OPERATIONS_HXX_
