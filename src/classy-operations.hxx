#ifndef CLASSY_OPERATIONS_HXX_
# define CLASSY_OPERATIONS_HXX_

# include "classy-operations.hh"

template<class ... Args>
struct InfixEval
{
    typedef ShuntingYard<EmptyStack, EmptyStack, Args ..., Nulltype>    base;
    typedef typename base::type                                         type;

    static constexpr type value = base::value;
};

template<class StackTot, class StackOp>
struct ShuntingYard<StackTot, StackOp, Nulltype>
{
    typedef Eval<>
};

template<class StackTot, class StackOp, class ... Args>
struct ShuntingYard<StackTot, StackOp, Add, Args ...>
{
    typedef Eval<typename
                ShuntingYard<
                    StackTot,
                    StackPush<
                        Add,
                        StackOp>,
                    Args ...>::instrs>                  base;
    typedef typename base::type                         type;

    static constexpr type value = base::value;
};

template<class StackTot, class StackOp, class ... Args>
struct ShuntingYard<StackTot, StackOp, Sub, Args ...>
{
    typedef Eval<typename
                ShuntingYard<
                    StackTot,
                    StackPush<
                        Sub,
                        StackOp>,
                    Args ...>::instrs>                  base;
    typedef typename base::type                         type;

    static constexpr type value = base::value;
};

template<class StackTot, class StackOp, class ... Args>
struct ShuntingYard<StackTot, StackOp, Mul, Args ...>
{
    typedef Eval<typename
                ShuntingYard<
                    StackTot,
                    StackPush<
                        Mul,
                        StackOp>,
                    Args ...>::value>                   base;
    typedef typename base::type                         type;

    static constexpr type value = base::value;
};

template<class StackTot, class StackOp, class ... Args>
struct ShuntingYard<StackTot, StackOp, Div, Args ...>
{
    typedef Eval<typename
                ShuntingYard<
                    StackTot,
                    StackPush<
                        Div,
                        StackOp>,
                    Args ...>::value>                   base;
    typedef typename base::type                         type;

    static constexpr type value = base::value;
};

template<class StackTot, class StackOp, class ... Args>
struct ShuntingYard<StackTot, StackOp, Mod, Args ...>
{
    typedef Eval<typename
                ShuntingYard<
                    StackTot,
                    StackPush<
                        Mod,
                        StackOp>,
                    Args ...>::value>                   base;
    typedef typename base::type                         type;

    static constexpr type value = base::value;
};

template<class StackTot, class StackOp, class ... Args>
struct ShuntingYard<StackTot, StackOp, Puiss, Args ...>
{
    typedef Eval<typename
                ShuntingYard<
                    StackTot,
                    StackPush<
                        Puiss,
                        StackOp>,
                    Args ...>::value>                   base;
    typedef typename base::type                         type;

    static constexpr type value = base::value;
};

template<class StackTot, class StackOp, class IntType, class ... Args>
struct ShuntingYard<StackTot, StackOp, IntType, Args ...>
{
    typedef Eval<typename
                ShuntingYard<
                    StackPush<
                        IntType,
                        StackTot>,
                    StackOp,
                    Args ...>::value>                   base;
    typedef typename base::type                         type;

    static constexpr type value = base::value;
};

template<class ... Args>
struct Eval<Add, Args ...>
{
    typedef typename WithTwoOperands<Args ...>::base    base;
    typedef typename WithTwoOperands<Args ...>::type    type;
    typedef typename WithTwoOperands<Args ...>::rest    rest;

    static constexpr type value = base::value + base::rest::value;

    static constexpr bool is_op = true;
    static constexpr bool is_num = false;

    static constexpr char priority = 1;
};

template<class ... Args>
struct Eval<Sub, Args ...>
{
    typedef typename WithTwoOperands<Args ...>::base    base;
    typedef typename WithTwoOperands<Args ...>::type    type;
    typedef typename WithTwoOperands<Args ...>::rest    rest;

    static constexpr type value = base::value - base::rest::value;

    static constexpr bool is_op = true;
    static constexpr bool is_num = false;

    static constexpr char priority = 1;
};

template<class ... Args>
struct Eval<Mul, Args ...>
{
    typedef typename WithTwoOperands<Args ...>::base    base;
    typedef typename WithTwoOperands<Args ...>::type    type;
    typedef typename WithTwoOperands<Args ...>::rest    rest;

    static constexpr type value = base::value * base::rest::value;

    static constexpr bool is_op = true;
    static constexpr bool is_num = false;

    static constexpr char priority = 2;
};

template<class ... Args>
struct Eval<Div, Args ...>
{
    typedef typename WithTwoOperands<Args ...>::base    base;
    typedef typename WithTwoOperands<Args ...>::type    type;
    typedef typename WithTwoOperands<Args ...>::rest    rest;

    static type constexpr value = base::value / base::rest::value;

    static constexpr bool is_op = true;
    static constexpr bool is_num = false;

    static constexpr char priority = 2;
};

template<class ... Args>
struct Eval<Mod, Args ...>
{
    typedef typename WithTwoOperands<Args ...>::base    base;
    typedef typename WithTwoOperands<Args ...>::type    type;
    typedef typename WithTwoOperands<Args ...>::rest    rest;

    static type constexpr value = base::value % base::rest::value;

    static constexpr bool is_op = true;
    static constexpr bool is_num = false;

    static constexpr char priority = 2;
};

template<class ... Args>
struct Eval<Puiss, Args ...>
{
    typedef typename WithTwoOperands<Args ...>::base    base;
    typedef typename WithTwoOperands<Args ...>::type    type;
    typedef typename WithTwoOperands<Args ...>::rest    rest;

    static type constexpr value = puiss(base::value, base::rest::value);

    static constexpr bool is_op = true;
    static constexpr bool is_num = false;

    static constexpr char priority = 3;
};

template<class NumType, class ... Args>
struct Eval<NumType, Args ...>
{
    typedef typename NumType::type                      type;
    typedef Eval<Args ...>                              rest;

    static constexpr type value = NumType::value;

    static constexpr bool is_op = false;
    static constexpr bool is_num = true;
};

template<class ... Args>
struct WithOperands
{
    typedef Eval<Args ...>                              base;
    typedef typename base::type                         type;
};

template<class ... Args>
struct WithOneOperand
{
    typedef typename WithOperands<Args ...>::base       base;
    typedef typename WithOperands<Args ...>::type       type;
    typedef typename base::rest                         rest;
};

template<class ... Args>
struct WithTwoOperands
{
    typedef typename WithOperands<Args ...>::base       base;
    typedef typename WithOperands<Args ...>::type       type;
    typedef typename base::rest::rest                   rest;
};

template<int N>
struct Int
{
    typedef int type;

    static constexpr type value = N;
};

template<int N>
struct Float
{
    typedef float type;

    static constexpr type value = N;
};

template<int N>
struct Double
{
    typedef double type;

    static constexpr type value = N;
};

#endif // CLASSY_OPERATIONS_HXX_
