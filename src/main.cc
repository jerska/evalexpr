#include <classy-operations.hh>
#include <iostream>

constexpr float my_calc()
{
    return Eval<Sub, Add, Div, Mul, Sub, Add, Eval<Add, Double<2>, Int<4>>, Double<4>, Float<3>, Double<10>, Double<4>, Int<4>, Double<22>>::value;
}

constexpr bool check_validity()
{
    return my_calc() == -0.5;
}

int main()
{
    static_assert(check_validity() , "Not at compile time");
    std::cout << my_calc() << std::endl;
}
