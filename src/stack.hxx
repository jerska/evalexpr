#ifndef STACK_HXX_
# define STACK_HXX_

# include "stack.hh"

template <class ... Args>
struct Stack
{
};

template <class C, class ... Args>
struct StackPush<C, Args ...>
{
    typedef Stack<C, Args ...>                              stack;
};

template <class C, class ... Args>
struct StackPop<C, Args ...>
{
    typedef C                                               elem;
    typedef Stack<Args ...>                                 stack;
};

#endif // STACK_HXX_
