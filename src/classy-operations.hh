#ifndef CLASSY_OPERATIONS_HH_
# define CLASSY_OPERATIONS_HH_

# include <stack.hh>

template<class ... Args>
struct InfixEval;

template<class StackTot, class StackOp, class ... Args>
struct ShuntingYard;

template<class ... Args>
struct Eval;

template<class ... Args>
struct WithOperands;

template<class ... Args>
struct WithOneOperand;

template<class ... Args>
struct WithTwoOperands;

template<int N>
struct Int;

template<int N>
struct Float;

template<int N>
struct Double;

class Add;
class Sub;
class Mul;
class Div;
class Mod;
class Puiss;

class Nulltype;

# include "classy-operations.hxx"

#endif // CLASSY_OPERATIONS_HH_
